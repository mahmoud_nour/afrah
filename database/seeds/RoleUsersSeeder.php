<?php

use Illuminate\Database\Seeder;

class RoleUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
            array('
            slug'=>'admin',
            'name'=>'Administrator',
            'permissions'=>'{
                "admin.show":true,
                "admin.edit":true,
                "admin.delete":true,
                "admin.create":true,
                "admin.approve":true}'),
            array(
            'slug'=>'moderator',
            'name'=>'moderator',
            'permissions'=>'{
                "moderator.show":true,
                "moderator.edit":true,
                "moderator.delete":false,
                "moderator.create":true,
                "moderator.approve":false}'),
            array(
            'slug'=>'user',
            'name'=>'Normal User',
            'permissions'=>'{
                "user.show":true,
                "user.edit":false,
                "user.delete":false,
                "user.create":false,
                "user.approve":false}'),
        ));
    }
}
