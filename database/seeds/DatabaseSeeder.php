<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        factory('App\User',10)->create();
        factory('App\Admin',1)->create();
        $this->call(UsersTableSeeder::class,10);
        $this->call(RolesTableSeeder::class);
        $this->call(AdminsTableSeeder::class,10);
    }
}
