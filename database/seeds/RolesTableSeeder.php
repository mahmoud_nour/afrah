<?php

use Illuminate\Database\Seeder;

use App\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Role();
        $user->name = 'admin';
        $user->slug = 'admin';
        $user->save();

        $user = new Role();
        $user->name = 'editor';
        $user->slug = 'editor';
        $user->save();

        $user = new Role();
        $user->name = 'user';
        $user->slug = 'user';
        $user->save();
    }
}
