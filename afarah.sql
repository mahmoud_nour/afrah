-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2018 at 07:46 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afarah`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'QdWInuvYYaK44f59D3d1PYmwfuCiNpQplnsejNzrD1PLIEHII4HMzQ9uMKak', '2018-03-18 07:19:12', '2018-03-18 07:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'الرياض', 'alryad', '2018-03-20 22:22:50', '2018-03-20 22:22:50'),
(2, 'ابها', 'abha', '2018-03-20 22:22:55', '2018-03-20 22:22:55'),
(3, 'مكه', 'mkh', '2018-03-20 22:22:58', '2018-03-20 22:22:58'),
(4, 'المدينة', 'almdyn', '2018-03-20 22:23:05', '2018-03-20 22:23:05'),
(5, 'الاسكندرية', 'alaskndry', '2018-03-20 22:24:27', '2018-03-20 22:24:27'),
(6, 'القاهره', 'alkahrh', '2018-03-20 22:24:31', '2018-03-20 22:24:31'),
(7, 'كفر الزيات', 'kfr-alzyat', '2018-03-20 22:24:39', '2018-03-20 22:24:39'),
(8, 'كفر الشيخ', 'kfr-alshykh', '2018-03-20 22:24:43', '2018-03-20 22:24:43'),
(9, 'طنطا', 'tnta', '2018-03-20 22:24:47', '2018-03-20 22:24:47');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'مصر', 'msr', '2018-03-20 22:23:19', '2018-03-20 22:23:19'),
(2, 'السعودية', 'alsaaody', '2018-03-20 22:23:24', '2018-03-20 22:23:24'),
(3, 'الامارات', 'alamarat', '2018-03-20 22:23:28', '2018-03-20 22:23:28'),
(4, 'الاردن', 'alardn', '2018-03-20 22:23:32', '2018-03-20 22:23:32'),
(5, 'البحرين', 'albhryn', '2018-03-20 22:23:40', '2018-03-20 22:23:40'),
(6, 'الكويت', 'alkoyt', '2018-03-20 22:23:43', '2018-03-20 22:23:43'),
(7, 'قطر', 'ktr', '2018-03-20 22:23:47', '2018-03-20 22:23:47');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `path`, `created_at`, `updated_at`) VALUES
(1, 'preview_4ooLXbIDaeeAXDnRUhS8yafx3.jpg', '/uploads/service_images/', '2018-03-20 22:32:41', '2018-03-20 22:32:41'),
(2, 'preview_7O055OLDmzuIwfvKpRAq0NRxi.jpg', '/uploads/service_images/', '2018-03-20 22:32:41', '2018-03-20 22:32:41'),
(3, 'preview_945577_386909178086785_865735323_n.jpg', '/uploads/service_images/', '2018-03-20 22:32:41', '2018-03-20 22:32:41'),
(4, 'preview_abricxlxycltcfvfohpaoeknz.jpg', '/uploads/service_images/', '2018-03-20 22:32:41', '2018-03-20 22:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2014_10_12_000000_create_users_table', 1),
(30, '2014_10_12_100000_create_password_resets_table', 1),
(31, '2018_03_03_042731_create_admins_table', 1),
(32, '2018_03_04_232222_create_roles_table', 1),
(33, '2018_03_04_232249_create_role_admins_table', 1),
(34, '2018_03_05_235525_create_images_table', 1),
(35, '2018_03_05_235551_create_profiles_table', 1),
(36, '2018_03_05_235626_create_categories_table', 1),
(37, '2018_03_05_235924_create_sections_table', 1),
(38, '2018_03_06_000019_create_countries_table', 1),
(39, '2018_03_06_000043_create_cities_table', 1),
(40, '2018_03_06_000048_create_services_table', 1),
(41, '2018_03_06_000049_create_service_image_table', 1),
(42, '2018_03_18_081500_create_roles_permissions_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `lable`, `created_at`, `updated_at`) VALUES
(1, 'edit_topic', 'edit the topic', '2018-03-18 10:43:09', '2018-03-18 10:43:09'),
(2, 'delete_topic', 'user can delete topic', '2018-03-18 23:11:19', '2018-03-18 23:11:19'),
(4, 'delete_topic', 'able to delete topic', '2018-03-19 16:49:11', '2018-03-19 16:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `lable`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'The admin of system', '2018-03-18 07:19:13', '2018-03-18 07:19:13'),
(2, 'editor', 'editor', NULL, '2018-03-18 07:19:13', '2018-03-18 07:19:13'),
(3, 'user', 'user', NULL, '2018-03-18 07:19:13', '2018-03-18 07:19:13'),
(4, 'moderator', NULL, 'The moderator of system', '2018-03-18 10:34:09', '2018-03-18 10:34:09'),
(5, 'super admin', NULL, 'the super admin', '2018-03-18 23:09:42', '2018-03-18 23:09:42'),
(6, 'super admin', NULL, 'the super admin', '2018-03-19 16:47:55', '2018-03-19 16:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `role_admins`
--

CREATE TABLE `role_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_admins`
--

INSERT INTO `role_admins` (`id`, `role_id`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-03-17 22:00:00', '2018-03-17 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'قاعات وفنادق', 'kaaaat-ofnadk', '2018-03-20 22:16:21', '2018-03-20 22:16:21'),
(2, 'استراحة', 'astrah', '2018-03-20 22:16:29', '2018-03-20 22:16:29'),
(3, 'الماء والعطر', 'alma-oalaatr', '2018-03-20 22:16:37', '2018-03-20 22:16:37'),
(4, 'ماذون شرعي', 'mathon-shraay', '2018-03-20 22:16:44', '2018-03-20 22:16:44'),
(5, 'السفر والسياحة', 'alsfr-oalsyah', '2018-03-20 22:16:51', '2018-03-20 22:16:51'),
(6, 'منتجعات', 'mntjaaat', '2018-03-20 22:16:58', '2018-03-20 22:16:58'),
(7, 'ساليهات', 'salyhat', '2018-03-20 22:17:03', '2018-03-20 22:17:03');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `hall` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `C_register` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `hall`, `slug`, `user_id`, `section_id`, `country_id`, `city_id`, `price`, `area`, `street`, `address`, `phone`, `email`, `web`, `C_register`, `notes`, `created_at`, `updated_at`) VALUES
(1, 'قاعة الأمير سلطان', 'kaaa-alamyr-sltan', 19, 1, 2, 2, 1900, 'المنسك', 'جهة طريق الخميس', 'جهة طريق الخميس', '0096987654321', 'admin@gmail.com', 'http://www.google.com', 'test', 'test', '2018-03-20 22:32:41', '2018-03-20 22:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `service_image`
--

CREATE TABLE `service_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `image_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_image`
--

INSERT INTO `service_image` (`id`, `service_id`, `image_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 1, 3, NULL, NULL),
(4, 1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_mob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(4) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verifytoken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone_number`, `verify_mob`, `verified`, `status`, `is_admin`, `email`, `password`, `verifytoken`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Weston Bauch DVM', NULL, NULL, 1, 1, 0, 'gusikowski.marianne@example.net', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'oeCImwYK6H', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(2, 'Ms. Cassandra Paucek MD', NULL, NULL, 1, 1, 0, 'fisher.daniela@example.org', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'LCyzHdOVi4', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(3, 'Audra Beahan PhD', NULL, NULL, 1, 0, 0, 'waters.claude@example.org', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'IUVWQluGb9', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(4, 'Adrain Hayes', NULL, NULL, 1, 0, 0, 'ngoldner@example.com', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'X7MfcunajA', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(5, 'Mr. Eduardo Wiegand Sr.', NULL, NULL, 1, 0, 0, 'mueller.dolly@example.com', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'WkuNd26MLV', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(6, 'Barton Schneider', NULL, NULL, 1, 0, 0, 'konopelski.cicero@example.net', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'pILYvuGXTc', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(7, 'Naomi Reinger III', NULL, NULL, 1, 0, 0, 'slang@example.net', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'Ll2cmA3M2E', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(8, 'Mrs. Aliya Corkery', NULL, NULL, 1, 0, 0, 'edgardo.bartell@example.net', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, 'LZgf8NNse9', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(9, 'Miss Eloise Donnelly IV', NULL, NULL, 1, 0, 0, 'hipolito92@example.net', '$2y$10$Vz.rKarKX2zhfr5d4HahpOrYnbW44agh/E2Z2ns2KfedkfbRYEFeC', NULL, '96nnTnIdvN', '2018-03-18 07:19:12', '2018-03-21 03:51:39'),
(19, 'mido', '201011966535', NULL, 1, 0, 0, 'mido@test.com', '$2y$10$dTcoiUHzK/6XXgS2RvALyuLsManAVzWCz5PAD0RDy9KeeHjOOK85K', NULL, 'ww3l0fIiYSByUeGggkoOt5P69NiH0Le1nPxKPKlkiUEMFUiu8vHSIvFfI4KM', '2018-03-20 21:50:31', '2018-03-21 03:51:39'),
(30, 'mahmoud', '+201147170572', 'zNUKu', 0, 0, 0, 'mahmoud2@test.com', '$2y$10$C1LV8BdJpS5BQrM08UBUCuPT1EYyAvVFrBraH8zoLgvqIq0lk/TpC', NULL, NULL, '2018-03-21 04:43:59', '2018-03-21 04:43:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_slug_unique` (`slug`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `countries_slug_unique` (`slug`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_role_role_id_index` (`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_admins`
--
ALTER TABLE `role_admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_admins_role_id_index` (`role_id`),
  ADD KEY `role_admins_admin_id_index` (`admin_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sections_slug_unique` (`slug`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_slug_unique` (`slug`),
  ADD KEY `services_user_id_index` (`user_id`),
  ADD KEY `services_section_id_index` (`section_id`),
  ADD KEY `services_country_id_index` (`country_id`),
  ADD KEY `services_city_id_index` (`city_id`);

--
-- Indexes for table `service_image`
--
ALTER TABLE `service_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_image_service_id_index` (`service_id`),
  ADD KEY `service_image_image_id_index` (`image_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role_admins`
--
ALTER TABLE `role_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `service_image`
--
ALTER TABLE `service_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_admins`
--
ALTER TABLE `role_admins`
  ADD CONSTRAINT `role_admins_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_admins_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `services_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `services_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
  ADD CONSTRAINT `services_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `service_image`
--
ALTER TABLE `service_image`
  ADD CONSTRAINT `service_image_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_image_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
