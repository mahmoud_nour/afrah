@extends('front.index')
@section('style')
<link rel="stylesheet" href="{{ url('front') }}/css/ekko-lightbox.css">
@endsection

@section('content')
<!-- breadcrumb  -->
<main class="bg-breadcrumb">
	<div class="container">
		<nav  aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="{{ url('front') }}/index.html">الرئيسية</a></li>
		    <li class="breadcrumb-item active" aria-current="page">منسق حفلات القصيم</li>
		  </ol>
		</nav>
	</div>
</main>
<!-- Content  -->
{{-- {{ dd($service[0]) }} --}}

  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="main-content">
          <!-- العنوان   -->
          <h4> منسق حفلات القصيم</h4>
          <!--  يجب وضع تقييم في البرمجة من خلال لوحة التحكم   -->
          <img src="{{ url('front') }}/img/rate.png">
          <img src="{{ url('front') }}/img/rate.png">
          <img src="{{ url('front') }}/img/rate.png">
          <img src="{{ url('front') }}/img/rate.png">
          <!--  المكان  -->
          <span class="area"> </span>
          <!-- الصور  -->
					<main class="main-img">
						<div class="master">
							<a href="{{ url('front') }}/img/benches.jpg" data-toggle="lightbox" data-gallery="roadtrip">
								<img src="{{ url('front') }}/img/benches.jpg" class="img-fluid img-hid">
							</a>
						</div>
						<div class="justify-content-center">
							<a href="{{ url('front') }}/img/benches.jpg" data-toggle="lightbox" data-gallery="roadtrip">
					    		<img src="{{ url('front') }}/img/benches.jpg" class="img-fluid img-hid">
							</a>
							<a href="{{ url('front') }}/img/bridge.jpg" data-toggle="lightbox" data-gallery="roadtrip">
					    		<img src="{{ url('front') }}/img/bridge.jpg" class="img-fluid img-hid">
							</a>
							<a href="{{ url('front') }}/img/coast.jpg" data-toggle="lightbox" data-gallery="roadtrip">
					    		<img src="{{ url('front') }}/img/coast.jpg" class="img-fluid img-hid">
							</a>		            		
						</div>
					</main>
					<!-- بيانات تفصيلية	  -->

						<div class="table-section">
							<h4>بيانات تفصيلية اخرى </h4>
							<table class="table table-bordered">
							  <tbody>
									<tr>
										<th scope="row">السعر <i class="fas fa-dollar-sign"></i></th>
										<td> ريال</td>
									</tr>
							    <tr>
							      <th scope="row">الحي <i class="far fa-compass"></i></th>
							      <td></td>
							    </tr>
							    <tr>
							      <th scope="row">الشارع <i class="fas fa-road"></i></th>
							      <td></td>
							    </tr>
									<tr>
										<th scope="row">الهاتف <i class="fas fa-phone"></i></th>
										<td ></td>
									</tr>
									<tr>
										<th scope="row">البريد الإلكتروني	  <i class="fas fa-envelope"></i></th>
										<td ></td>
									</tr>
									<tr>
										<th scope="row">صفحة الإنترنت	 <i class="fas fa-globe"></i></th>
										<td ></td>
									</tr>
									<tr>
										<th scope="row">ملاحظات  <i class="fas fa-sticky-note"></i></th>
										<td ></td>
									</tr>
							  </tbody>
							</table>
						</div>
          </div>
          
        </div><!-- End  col 8 -->
		<div class="col-md-4">
			<div class="sidebar">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3629.8623592206723!2d46.650036985003894!3d24.524844684213317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjTCsDMxJzI5LjQiTiA0NsKwMzgnNTIuMyJF!5e0!3m2!1sar!2siq!4v1519582840909" width="600" height="450" frameborder="0" style="border:0" allowfullscreen>
				</iframe>
			</div>
			<div class="rate">
					<h3>تقييمات  <img src="img/rating.png" alt=""> </h3>
					<span>التعامل : ممتاز 8</span>
					<div class="progress">
					  <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<span>المصداقية : رائع 9</span>
					<div class="progress">
					  <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<span>حالة  : جيد جدا 7.5</span>
					<div class="progress">
					  <div class="progress-bar bg-info" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
			</div>
			<div class="share text-center">
				<p>شارك عبر </p>
				<a href="#"> <img src="img/facebook.png" alt=""> </a>
				<a href="#"> <img src="img/twitter.png" alt=""> </a>
				<a href="#"> <img src="img/whatsapp.png" alt=""> </a>
			</div>
		</div>
    </div><!-- End  Row -->
  </div><!-- End  container -->
<!-- منتجات دات صلة   -->
<div class="main-text3">
	<div class="container text3">
		<h3>خدمات ذات صلة </h3>
	</div>
</div>

@include('front.layouts.news')
@endsection

@section('script')
{{-- <script src="js/ekko-lightbox.min.js"></script> --}}
 <script src="{{ url('front') }}/js/ekko-lightbox.min.js"></script>
<script type="text/javascript">
			$(document).on('click', '[data-toggle="lightbox"]', function(event) {
							event.preventDefault();
							$(this).ekkoLightbox();
				});
		</script>

@endsection