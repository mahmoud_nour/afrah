<div class="container text">
	<h3>احصل على العروض وخدمات لمناسباتك <img src="{{ url('front') }}/img/offer.png" alt=""></h3>
</div>

<section class="bg-form">
		<div class="container">
				<form class="uk-grid" uk-grid>
				    <div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">الدولة</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>السعودية</option>
			                <option>قطر</option>
			                <option>البحرين</option>
			                <option>الامارات</option>
			            </select>
			        </div>
						</div>
						<div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">المدينة</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>الرياض</option>
			                <option>ابها</option>
			                <option>مكة</option>
			                <option>المدينة لمنورة</option>
			            </select>
			        </div>
						</div>
						<div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">النوع</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>قصورافراح</option>
			                <option>استراحة</option>
			                <option>فنادق</option>
			            </select>
			            </select>
			        </div>
						</div>
						<div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">التاريخ ان وجد ( اليوم )</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>01</option>
			                <option>02</option>
			                <option>03</option>
			                <option>02</option>
			            </select>
			        </div>
						</div>
						<div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">الشهر</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>01</option>
			                <option>02</option>
			                <option>03</option>
			                <option>04</option>
			            </select>
			        </div>
						</div>
						<div class="uk-width-1-3@s">
							<label class="uk-form-label" for="form-horizontal-select">السنة</label>
			        <div class="uk-form-controls">
			            <select class="uk-select" id="form-horizontal-select">
			                <option>1439</option>
			                <option>1440</option>
			                <option>1441</option>
			                <option>1442</option>
			            </select>
			        </div>
						</div>
						<div class="col-md-12 Search-form">
						      <input class="form-control col-md-9" type="text" placeholder="كلمة البحث" aria-label="Search">
						      <button class="btn btn-success col-md-3" type="submit">ابحث</button>
						</div>
				</form>
			</div>
		</div><!-- End container-->
</section>
<!-- Text the Best Website  -->
<div class="the-best text-center">
		<p>الموقع الأفضل للبحث عن مكان وخدمات مناسباتك	</p>
</div>
