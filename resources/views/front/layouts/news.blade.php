<div class="main-text3">
	<div class="container text3">
		<h3>جديدنا <img src="{{ url('front') }}/img/new.png" alt=""></h3>
	</div>
</div>

<section class="news">
	<div class="container">
		<div class="row">
		<div class="col-md-6">
			<a href="#">
				<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
				    <div class="uk-card-media-left uk-cover-container">
				        <img src="{{ url('front') }}/img/2.png" alt="" uk-cover>
				        <canvas width="600" height="400"></canvas>
				    </div>
		        <div class="uk-card-body">
		            <h3 class="uk-card-title">منسق حفلات القصيم</h3>
		            <p>سعودية , الرياض</p>
								<div class="price">1000 ريال</div>
		        </div>
				</div>
			</a>
		</div>
		<div class="col-md-6">
			<a href="#">
				<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
				    <div class="uk-card-media-left uk-cover-container">
				        <img src="{{ url('front') }}/img/2.png" alt="" uk-cover>
				        <canvas width="600" height="400"></canvas>
				    </div>
		        <div class="uk-card-body">
		            <h3 class="uk-card-title">منسق حفلات القصيم</h3>
		            <p>سعودية , الرياض</p>
								<div class="price">1000 ريال</div>
		        </div>
				</div>
			</a>
		</div>
		<div class="col-md-6">
			<a href="#">
				<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
				    <div class="uk-card-media-left uk-cover-container">
				        <img src="{{ url('front') }}/img/2.png" alt="" uk-cover>
				        <canvas width="600" height="400"></canvas>
				    </div>
		        <div class="uk-card-body">
		            <h3 class="uk-card-title">منسق حفلات القصيم</h3>
		            <p>سعودية , الرياض</p>
								<div class="price">1000 ريال</div>
		        </div>
				</div>
			</a>
		</div>
		<div class="col-md-6">
			<a href="#">
				<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
				    <div class="uk-card-media-left uk-cover-container">
				        <img src="{{ url('front') }}/img/2.png" alt="" uk-cover>
				        <canvas width="600" height="400"></canvas>
				    </div>
		        <div class="uk-card-body">
		            <h3 class="uk-card-title">منسق حفلات القصيم</h3>
		            <p>سعودية , الرياض</p>
								<div class="price">1000 ريال</div>
		        </div>
				</div>
			</a>
		</div>
			<a class="watchall" href="#">شاهد جميع العروض</a>
	</div><!-- End row  -->
	</div><!-- End container  -->

</section>