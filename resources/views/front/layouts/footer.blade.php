<footer>
<div class="container">
	<div class="row">
		<div class="col-md-12">
				<div class="up-footer">
					<div class="col-sm-6">
						<a class="navbar-brand" href="#">أفـــــــــراح</a>
						<div><label for="exampleInputEmail1">اشترك لتصلك العروض والخصومات </label></div>
						<div class="input-group">
					  	<input type="text" class="form-control" placeholder="البريد الالكتروني" >
					  <div class="input-group-append">
					    <button class="btn btn-info" type="button">اشتراك</button>
					  </div>
					</div>
					</div>
					<div class="col-sm-6">
						<div class="social text-center">
							<h3>تابعنا</h3>
							<a href="#"> <img src="{{ url('front') }}/img/facebook.png" alt=""> </a>
							<a href="#"> <img src="{{ url('front') }}/img/twitter.png" alt=""> </a>
							<a href="#"> <img src="{{ url('front') }}/img/instagram1.png" alt=""> </a>
							<a href="#"> <img src="{{ url('front') }}/img/snapchat.png" alt=""> </a>
							<a href="#"> <img src="{{ url('front') }}/img/whatsapp.png" alt=""> </a>

						</div>
					</div>
				</div>
		</div>
	</div>
</footer>
