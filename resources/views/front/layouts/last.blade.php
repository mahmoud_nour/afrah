<!-- The Last Footer  -->
<div class="last">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
						<div class="col-md-4">
							<div class="down-footer">© 2018 جميع الحقوق محفوظة لمؤسسة الخدمة المريحة <img src="{{ url('front') }}/img/happy.png" alt=""></div>
						</div>
						<div class="col-md-8">
							<div class="icon-footer">
								<a href="aboutus.html"> <i class="fas fa-exclamation-circle"></i> عن الموقع </a>
								<a href="bankaccount.html"><i class="fas fa-credit-card"></i> الحسابات النبكة</a>
								<a href="contactus.html"><i class="fas fa-phone-square"></i> اتصل بنا</a>
							</div>
						</div>
				</div>
		</div>
	</div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="{{ url('front') }}/js/uikit.min.js"></script>
    <script src="{{ url('front') }}/js/uikit-icons.min.js"></script>
    <script src="{{ url('front') }}/js/custom.js"></script>
	@yield('script')
  </body>
  </html>