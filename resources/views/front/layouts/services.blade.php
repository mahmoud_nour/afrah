@if (count($services) > 0) 
<div class="main-text">
	<div class="container text2">
		<h3>خدمات  <img src="{{ url('front') }}/img/vip.png" alt=""></h3>
	</div>
</div>
	<!-- Pro  -->
<section class="bg-pro">
	<div class="container">
		<div class="row">
			{{-- <div class="col-md-6 clearfix">
					<div class="card ">
					  <img class="card-img-top" src="{{ url('front') }}/img/2.png" alt="Card image cap">
						<span class="ta">تخفيضات تصل 50% </span>
						<div class="card-body">
							 <div class="title">
								 <h5>منسق حفلات القصيم	</h5>
								 <span>سعودية , الرياض </span>
								 <div><img  src="{{ url('front') }}/img/rate.png"></div>
							 </div>
							 <div class="more">
								 <div class="price">1000 ريال</div>
								 <a class="btn btn-info" href="fisrt.html">تفاصيل اكثر</a>
							 </div>
						 </div>
					</div>
			</div>   --}}


				@foreach ($services as $service1)
				<div class="col-md-6 clearfix">
					<div class="card ">
						{{-- {{ dd($service1->images()->first()->name) }} --}}
						@if (count($service1->images()->first()['name']) !=0)
					  <img class="card-img-top"
					   src="{{url('/uploads/service_images').'/'.$service1->images()->first()['name'] }}" alt="Card image cap">
						@else
						<p>no images</p>
						@endif

						<div class="card-body">
							 <div class="title">
								 <h5>{{ $service1->hall }}</h5>
								 <span>{{ $service1->area }} </span>
								 {{-- {{ dd($service1->images()->first()->path) }} --}}
								 <div>
								 	<img  src=""></div>
							 </div>
							 <div class="more">
								 <div class="price">{{ $service1->price }} ريال</div>
								 <a class="btn btn-info" href="{{url('/service/').'/'.$service1->id }}">تفاصيل اكثر</a>
							 </div>
						 	</div>
					</div>
				</div>
				@endforeach

			<a class="watchall" href="">شاهد جميع العروض</a>
			@else
			<div class="col-md-12 clearfix text-center">
				<h1>لا توجد خدمات في الموقع حاليا</h1>
			</div>
			@endif
	</div><!-- End row-- 	>
	</div><!-- End container-->
</section><br>