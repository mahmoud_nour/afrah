<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
		  <a class="navbar-brand" href="{{ url('/') }}">أفـــــــــراح</a>
			<a href="#offcanvas-slide" class="uk-button uk-button-default" uk-toggle> <img src="{{ url('front') }}/img/menu.png" alt=""></a>
		  <div class="collapse navbar-collapse">
		    <ul class="navbar-nav mr-auto">
	@if (Route::has('login')) 
	    @auth
    		      <li class="nav-item">
                <a class="btn btn-primary" href="{{ url('addservice') }}">خدمة جديدة <i class="fas fa-plus"></i></a>
    		      </li>
			<div class="dropdown">
			   <img   id="dropdownMenuButton" data-toggle="dropdown" src="{{ url('front') }}/img/por1.png" alt="">
			  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" href="#">خدماتي <i class="fas fa-tasks"></i></a>
			    <a class="dropdown-item" href="{{ url('/user-settings') }}">اعدادات الحساب <i class="fas fa-cog"></i></a><hr class="hr-menu">
			    <a class="dropdown-item" href="{{ route('admin.logout') }}"
			        onclick="event.preventDefault();
			                 document.getElementById('logout-form').submit();">
			        تسجيل خروج
			    </a>
			    <form id="logout-form" action="{{ 'App\Admin' == Auth::getProvider()->getModel() ? route('admin.logout') : route('logout') }}"
			     method="POST" style="display: none;">
			    {{ csrf_field() }}
			    </form>
			  </div>
			</div>
	      @else
			<li class="nav-item">
			  <a class="btn btn-danger" href="{{ route('register') }}">انشاء حساب <i class="fas fa-user"></i></a>
			</li>
			<li class="nav-item">
			  <a class="btn btn-success" href="{{ route('login') }}">تسجيل دخول <span> <i class="fas fa-sign-in-alt"></i></span> </a>
			</li>
	    @endauth
 
	@endif		      
		    </ul>
		  </div>
		</div>
	</nav>


	<div id="offcanvas-slide" uk-offcanvas="flip: true; overlay: true">
	    <div class="uk-offcanvas-bar">

	        <ul class="uk-nav uk-nav-default">
						<h5>اختر القسم المناسب  <img src="{{ url('front') }}/img/section.png" alt=""></h5>
	            <li><a href="Sections.html">قصورافراح</a></li>
	            <li><a href="#">استراحات</a></li>
	            <li><a href="#">قاعات وفنادق</a></li>
	            <li><a href="#">منتجعات</a></li>
	            <li><a href="#">ساليهات</a></li>
	            <li><a href="#">مطابخ</a></li>
	            <li><a href="#">توريد</a></li>
	            <li><a href="#">حلا وتوزيعات</a></li>
	            <li><a href="#">مصوري حفلات</a></li>
	            <li><a href="#">استديوهات تسجيل</a></li>
	            <li><a href="#">فرق انشاد</a></li>
	            <li><a href="#">شعراء</a></li>
	            <li><a href="#">شيلات </a></li>
	            <li><a href="#">منسقي الحفلات </a></li>
	            <li><a href="#">كوشوطولات استقبال</a></li>
	            <li><a href="#">تاجير سيارات</a></li>
	            <li><a href="#">مطابع وتصميم</a></li>
	            <li><a href="#">السفر والسياحة</a></li>
	            <li><a href="#">ماذون شرعي</a></li>
	            <li><a href="#">الماء والعصائر</a></li>
	        </ul>

	    </div>
	</div>	