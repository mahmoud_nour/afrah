<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>موقع افراح</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

  	<link rel="stylesheet" href="{{ url('front') }}/css/uikit.min.css" />
  	<link rel="stylesheet" href="{{ url('front') }}/css/bootstrap.css" />
	<link rel="stylesheet" href="{{ url('front') }}/css/bootstrap-rtl.min.css" media="screen">
  	<link type="text/css"  rel="stylesheet" href="{{ url('front') }}/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="https://www.fontstatic.com/f=fantezy" />
  	<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
	<link rel="Shortcut Icon" href="{{ url('front') }}/img/AfrahIcon.ico" />
	
	@yield('style')
</head>