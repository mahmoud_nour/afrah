@include('front.layouts.header')
<body class="rtl">

<!-- Navbar Menu -->
@include('front.layouts.nav')
<!-- End Navbar -->
@include('front.layouts.message')
@if (Session::has('message'))
    <div class="alert alert-warning text-center"> {{ Session::get('message') }} </div>
@endif
@yield('content')
<!-- footer  -->
@include('front.layouts.footer')
@include('front.layouts.last')
<!-- End footer  -->