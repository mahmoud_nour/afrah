@extends('front.index')

@section('content')
{{-- {{ dd($service) }} --}}
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="upload-files">
        <!-- will be used to show any messages -->
          {{-- @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
          @endif --}}
        <h3 class="text-center">اضافة خدمة جديدة</h3>
        <form action="{{  route('service.update',$service->id) }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">اسم القاعة *</label>
            <div class="col-sm-9">
              <input type="text" name="hall" class="form-control" value="{{$service->hall}}" placeholder="اسم القاعة ">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">السعر *</label>
            <div class="col-sm-9">
              <input type="text" name="price" class="form-control" value="{{$service->price}}" placeholder="السعر">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">القسم *</label>
            <div class="col-sm-9">
              <select id="section" name="section" class="form-control">
                {{-- <option selected>...</option> --}}
                <option value="{{$service->section}}" selected>{{$service->section}}</option>
                <option> اضافة باقي الاقسام </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الدولة *</label>
            <div class="col-sm-9">
              <select id="country" name="country" class="form-control">
                <option selected value="{{$service->country}}">{{$service->country}}</option>
                <option> قصور وافراح  </option>
                <option> اضافة باقي الاقسام </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">المدينة *</label>
            <div class="col-sm-9">
              <select id="city" name="city" class="form-control">
                <option selected value="{{$service->city}}">{{$service->city}}</option>
                <option> قصور وافراح  </option>
                <option> اضافة باقي الاقسام </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الحي</label>
            <div class="col-sm-9">
              <input type="text" name="area" class="form-control" value="{{$service->area}}"  placeholder="الحي">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الشارع</label>
            <div class="col-sm-9">
              <input type="text"  name="street" class="form-control" value="{{$service->street}}" placeholder="الشارع">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">العنوان *</label>
            <div class="col-sm-9">
              <input type="text" name="address" class="form-control" value="{{$service->address}}" placeholder="العنوان">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الهاتف *</label>
            <div class="col-sm-9">
              <input type="text" name="phone" class="form-control" value="{{$service->phone}}" placeholder="الهاتف">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">البريد الإلكتروني	 *</label>
            <div class="col-sm-9">
              <input type="text" name="email" class="form-control" value="{{$service->email}}" placeholder="البريد الإلكتروني	 ">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">صفحة الإنترنت	</label>
            <div class="col-sm-9">
              <input type="text" name="web" class="form-control" value="{{$service->web}}" placeholder="صفحة الإنترنت	">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">السجل التجاري	</label>
            <div class="col-sm-9">
              <input type="text" name="C_register" class="form-control" value="{{$service->C_register}}" placeholder="السجل التجاري	">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">ملاحظات </label>
            <div class="col-sm-9">
              <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="4">{{$service->notes}}</textarea>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label" style="font-size:13px;">ارفع صورة مصغرة لاعلان *</label>
              <div class="col-sm-9">
                  <div uk-form-custom="target: true">
                      <input type="file" name="images[]" multiple="true" class="form-control">
                      <input class="uk-input uk-form-width-medium" type="text" placeholder="اختصر صورة" >
                  </div>
              </div>
            </div>
					 <label>ضع صور للاعلان مالا يقل عن 4 صور</label>
          <br>
          <button type="submit" class="btn btn-primary ">تعديل اعلان </button>
        </form>
        <br>
        <div class="form-group row">
            <div class="col-sm-3">

              @if (count($service->images()->get()) !=0)
              <a href=""></a>
                <form action="{{  route('service.delete_image',$service->id) }}" method="post" accept-charset="utf-8">
                      <input type="hidden" name="_method" value="delete">
                      {{-- <input type="hidden" name="image_id" value="{{$image->id}}"> --}}
                      {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger"> حذف  الصور </button>
                </form>
              @endif

              <form action="{{  route('service.delete',$service->id) }}" method="post" accept-charset="utf-8">
                      <input type="hidden" name="_method" value="delete">
                      {{-- <input type="hidden" name="image_id" value="{{$image->id}}"> --}}
                      {{ csrf_field() }}
                      <br>
                    <button type="submit" class="btn btn-danger"> حذف الخدمة </button>
                </form>   
            </div>
           
            <div class="col-sm-9">
              @foreach ($service->images()->get() as $image)
                <img src="{{ url('/uploads/service_images').'/'.$image->name}}" id="{{ $image->id }}" class="img-responsive img-fluid img-hid" 
                    style="width:20%;">
              @endforeach
               </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection