@extends('front.index')

@section('content')
<main class="bg-breadcrumb">
    <div class="container">
        <nav  aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
            <li class="breadcrumb-item active" aria-current="page">المعلومات الشخصية</li>
          </ol>
        </nav>
    </div>
</main>

<!-- End Breadcrumb -->
{{-- @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif --}}
<!-- رمز التحقق  -->
<section class="var">
    
<div class="container text-center">
    <div class="row">
        <div class="col-md-12">
                <div class="main-doc">
                        <h4>رمز التحقق</h4>
                        <p>ادخل رمز التحقق المرسل لجوالك</p>
                        {{-- <span>966XXXXXXX+</span> --}}
                        <form class="" action="{{ url('nexmo') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_token" value='{{csrf_token()}}'>
                             @if ($errors->has('verify_mob'))
                                    <span class="help-block ">
                                        <strong>{{ $errors->first('verify_mob') }}</strong>
                                    </span>
                                @endif
                            <input type="text" name="verify_mob" class="form-control text-center" placeholder="ادخل رمز التحقق"  autocomplete="off">

                            <button type="submit" class="btn btn-primary ">ارسال </button>

                        </form>
                </div>
        </div>
    </div>
</div>
</section>

{{-- @include('front.layouts.news') --}}
<!-- End The News  -->
@endsection