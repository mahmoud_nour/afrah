@extends('front.index')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="upload-files">
        <!-- will be used to show any messages -->
{{-- @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif --}}

        <h3 class="text-center">اضافة خدمة جديدة</h3>
        <form action="addService" id="upload" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">اسم القاعة *</label>
            <div class="col-sm-9">
              <input type="text" name="hall" class="form-control" placeholder="اسم القاعة ">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">السعر *</label>
            <div class="col-sm-9">
              <input type="number" name="price" class="form-control" placeholder="السعر">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">القسم *</label>
            <div class="col-sm-9">
              <select id="section" name="section" class="form-control">
                @foreach ($sections as $section)
                <option value="{{ $section->id }}">{{ $section->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الدولة *</label>
            <div class="col-sm-9">
              <select id="country" name="country" class="form-control">
                @foreach ($countries as $country)
                  <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">المدينة *</label>
            <div class="col-sm-9">
              <select id="city" name="city" class="form-control">
                @foreach ($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الحي</label>
            <div class="col-sm-9">
              <input type="text" name="area" class="form-control"  placeholder="الحي">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الشارع</label>
            <div class="col-sm-9">
              <input type="text"  name="street" class="form-control"  placeholder="الشارع">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">العنوان *</label>
            <div class="col-sm-9">
              <input type="text" name="address" class="form-control"  placeholder="العنوان">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">الهاتف *</label>
            <div class="col-sm-9">
              <input type="text" name="phone" class="form-control"  placeholder="الهاتف">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">البريد الإلكتروني	 *</label>
            <div class="col-sm-9">
              <input type="text" name="email" class="form-control"  placeholder="البريد الإلكتروني	 ">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">صفحة الإنترنت	</label>
            <div class="col-sm-9">
              <input type="text" name="web" class="form-control"  placeholder="صفحة الإنترنت	">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">السجل التجاري	</label>
            <div class="col-sm-9">
              <input type="text" name="C_register" class="form-control"  placeholder="السجل التجاري	">
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label">ملاحظات </label>
            <div class="col-sm-9">
              <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="4"></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label  class="col-sm-3 col-form-label" style="font-size:13px;">ارفع صورة مصغرة لاعلان *</label>
              <div class="col-sm-9">
                  <div uk-form-custom="target: true">
                      <input type="file" name="images[]" multiple="true" class="form-control">
                      <input class="uk-input uk-form-width-medium" type="text" placeholder="اختصر صورة" >
                  </div>
              </div>
            </div>
					 <label>ضع صور للاعلان مالا يقل عن 4 صور</label>
{{--           <div class="form-group row">
            <label  class="col-sm-3 col-form-label"></label>
            <div class="col-sm-9">
                <div uk-form-custom="target: true">
                    <input type="file" name="image[]">
                    <input class="uk-input uk-form-width-medium" type="text"  placeholder="اختصر صورة" >
                </div>
            </div>
          </div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<div uk-form-custom="target: true">
									<input type="file" name="image[]">
									<input class="uk-input uk-form-width-medium" type="text"  placeholder="اختصر صورة" >
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<div uk-form-custom="target: true">
									<input type="file" name="image[]" >
									<input class="uk-input uk-form-width-medium" type="text" placeholder="اختصر صورة" >
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<div uk-form-custom="target: true">
									<input type="file" name="image[]">
									<input class="uk-input uk-form-width-medium" type="text"  placeholder="اختصر صورة" >
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<div uk-form-custom="target: true">
									<input type="file" name="image[]">
									<input class="uk-input uk-form-width-medium" type="text"  placeholder="اختصر صورة" >
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<div uk-form-custom="target: true">
									<input type="file" name="image[]">
									<input class="uk-input uk-form-width-medium" type="text"  placeholder="اختصر صورة" >
							</div>
						</div>
					</div> --}}
          <br>
          <button type="submit" class="btn btn-primary ">اضافة اعلان </button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection