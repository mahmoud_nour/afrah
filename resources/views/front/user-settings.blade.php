@extends('front.index')

@section('content')
<main class="bg-breadcrumb">
    <div class="container">
        <nav  aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">الرئيسية</a></li>
            <li class="breadcrumb-item active" aria-current="page">المعلومات الشخصية</li>
          </ol>
        </nav>
    </div>
</main>

<!-- End Breadcrumb -->

<!-- info porfile content -->

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="main-info">

        <div class="main-login">
        <form>
            <div class="modal-content">
                  <h4 class="title-info"> تعديل المعلومات الشخصية </h4>
                <div class="modal-body">
                  <div class="img-info text-center">
                        <img src="{{ url('front') }}/img/por1.png" alt="">
                        <input type="file">
                  </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-user"></i>
                            </span>
                            <input type="text" disabled  class="form-control" placeholder="اسم المستخدم " />
                        </div>
                    </div>
                    <hr>
                    <label>تحديث البريد الالكتروني</label>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input type="email"   class="form-control" placeholder=" البريد الالكتروني الجديد  " />
                        </div>
                    </div>
                      <hr>
                      <label> تحديث كلمة المرور</label>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-key"></i>
                            </span>
                            <input type="password"   class="form-control"  placeholder=" كلمة المرور الحالية " />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-key"></i>
                            </span>
                            <input type="password"   class="form-control"  placeholder="كلمة المرور الجديدة " />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-key"></i>
                            </span>
                            <input type="password"  class="form-control"  placeholder="تأكيد كلمة المرور الجديدة " />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">تحديث </button>
                </div>
            </div>
        </form>
        </div><!-- End main-login -->

      </div>
    </div>
  </div>
</div>

{{-- @include('front.layouts.news') --}}
<!-- End The News  -->
@endsection