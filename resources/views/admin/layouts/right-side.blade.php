<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>لوحه التحكم</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ count($services) }}</h3>
                        <p>عدد الخدمات</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ url('admin/services') }}" class="small-box-footer">
                        عرض الكل <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3> {{ count($users) }}</h3>
                        <p>عدد الاعضاء</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ url('admin/users') }}" class="small-box-footer">
                        عرض الكل <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ count($sections) }}</h3>
                        <p>الاقسام</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ url('admin/section') }}" class="small-box-footer">
                        عرض الكل <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div><!-- /.row -->
        <!-- top row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">
            </div><!-- /.col -->
        </div>
        <!-- /.row -->
    </section><!-- /.content -->
</aside><!-- /.right-side -->