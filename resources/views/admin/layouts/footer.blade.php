
        <!-- jQuery 2.0.2 -->
        <script src="{{ url('back') }}/ajax/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('back') }}/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('back') }}/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="{{ url('back') }}/js/raphael-min.js"></script>
        {{-- <script src="{{ url('back') }}/js/plugins/morris/morris.min.js" type="text/javascript"></script> --}}
        <!-- Sparkline -->
        <script src="{{ url('back') }}/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{ url('back') }}/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="{{ url('back') }}/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="{{ url('back') }}/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{ url('back') }}/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{ url('back') }}/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('back') }}/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{ url('back') }}/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('back') }}/js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        {{-- <script src="{{ url('back') }}/js/AdminLTE/dashboard.js" type="text/javascript"></script>      --}}
        
        <!-- AdminLTE for demo purposes -->
        {{-- <script src="{{ url('back') }}/js/AdminLTE/demo.js" type="text/javascript"></script> --}}

    </body>
</html>