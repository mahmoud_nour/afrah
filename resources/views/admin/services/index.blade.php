@include('admin.layouts.header')
@section('style')
 
@endsection
 <title>لوحة التحكم</title>
{{-- @include('layouts.nav') --}}
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            @include('admin.layouts.sidebar')
            <!-- /.sidebar -->
        </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
                    <li class="active">الخدمات</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                    {{-- all section --}}
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">الخدمات</h3>
                            <div class="box-footer pull-right">
                                <a href="{{ url('admin/services/add') }}" title="اضافة خدمة جديد">
                                    <button type="submit" class="btn btn-primary">اضافة خدمة جديد</button>
                                </a>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>الاسم</th>
                                    <th style="width: 40px">تعديل</th>
                                    <th style="width: 40px">حذف</th>
                                    <th style="width: 40px">نشر</th>
                                </tr>
                                @foreach ($services as $service)
                                <tr>
                                    <td>{{$loop->iteration}}.</td>
                                    <td>{{$service->hall}}</td>
                                    <td>
                                        <a href="{{ url('admin/services',$service->id) }}" title="تعديل">
                                            <span class="badge bg-orange">تعديل</span>
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.service.delete',$service->id) }}" method="post" accept-charset="utf-8">
                                             {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                              <button type="submit" class="sub badge bg-red"
                                              style=" border:none;outline:none;"
                                              >حذف</button>
                                        </form>
                                    </td>
                                    <td>
                                        @if ($service->publish >0)
                                      <p  class="sub badge bg-blue" style=" border:none;outline:none;">تم نشرة </p>
                                        @else
                                      <p  class="sub badge bg-yellow" style=" border:none;outline:none;">لم يتم نشرة</p>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    {{--/ all section --}}
                    </div><!--/.col (left) -->
                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->
@include('admin.layouts.footer')
