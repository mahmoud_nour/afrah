@include('admin.layouts.header')
@section('style')
    <title>لوحة التحكم</title>
@endsection
 <title>لوحة التحكم</title>
{{-- @include('layouts.nav') --}}
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                @include('admin.layouts.sidebar')
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
             <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
                        <li><a href="{{ url('admin/countries') }}">الدول</a></li>
                        <li class="active">تعديل الدولة</li>
                    </ol>
                </section>
                    @if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">تعديل القسم</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form action="{{  route('admin.country.Update',$country->id) }}"  method="post" role="form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="section">اسم القسم</label>
                                            <input type="text" class="form-control" name="name" id="section" placeholder="اسم القسم" value="{{old('name', $country->name)}}">
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
 

                        </div><!--/.col (left) -->
                        
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
@include('admin.layouts.footer')
