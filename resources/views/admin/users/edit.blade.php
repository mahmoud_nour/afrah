@include('admin.layouts.header')
@section('style')
    <title>لوحة التحكم</title>
@endsection
<script>
$(document).ready(function(){
    $('#role').change(function() {
        var role_val = $('#role').val();
        alert(role_val);
    });
});    
</script>
 <title>لوحة التحكم</title>
{{-- @include('layouts.nav') --}}
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                @include('admin.layouts.sidebar')
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
             <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
                        <li><a href="{{ url('admin/users') }}">الاعضاء</a></li>
                        <li class="active">تعديل العضو</li>
                    </ol>
                </section>
                    @if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">تعديل العضو</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form action="{{  route('admin.user.Update',$user->id) }}"  method="post" role="form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="box-body">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label for="section">اسم العضو</label>
                                            <input type="text" class="form-control" name="name" placeholder="اسم العضو" value="{{old('name', $user->name)}}">
                                            <label for="section">بريد الالكتروني</label>
                                            <input type="email" class="form-control" name="email" placeholder="بريد الالكتروني" value="{{old('name', $user->email)}}" autocomplete="off">
                                            <label for="section">الرقم السري الجديد</label>
                                            <input type="password" class="form-control" name="password_confirmation" placeholder="الرقم السري الجديد"  autocomplete="off">
                                            <label for="section">حالة العضو</label>
                                            <select name="status" class="form-control">
                                                <option value="1" {{old('status',$user->status)=="1"? 'selected':''}} >مفعل</option>
                                                <option value="0" {{old('status',$user->status)=="0"? 'selected':''}} >غير مفعل</option>
                                            </select>
                                            <label for="section">حالة العضو</label>
                                            <select name="is_admin" class="form-control" id="role">
                                                <option value="1" {{old('is_admin',$user->is_admin)=="1"? 'selected':''}} >مفعل</option>
                                                <option value="0" {{old('is_admin',$user->is_admin)=="0"? 'selected':''}} >غير مفعل</option>
                                            </select>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">تعديل</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
 

                        </div><!--/.col (left) -->
                        
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
@include('admin.layouts.footer')
