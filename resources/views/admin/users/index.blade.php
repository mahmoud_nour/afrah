@include('admin.layouts.header')
@section('style')
 
@endsection
 <title>لوحة التحكم</title>
{{-- @include('layouts.nav') --}}
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="left-side sidebar-offcanvas">
            <!-- sidebar: style can be found in sidebar.less -->
            @include('admin.layouts.sidebar')
            <!-- /.sidebar -->
        </aside>
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/home') }}"><i class="fa fa-dashboard"></i> لوحة التحكم</a></li>
                    <li class="active">الاعضاء</li>
                </ol>
            </section>
            @if (Session::has('message'))
                        <div class="alert alert-warning">{{ Session::get('message') }}</div>
            @endif
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                    {{-- all section --}}
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">الاعضاء</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>الاسم</th>
                                    <th style="width: 40px">تعديل</th>
                                    <th style="width: 40px">حذف</th>
                                    <th style="width: 40px">صلاديات العضو</th>
                                </tr>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{$loop->iteration}}.</td>
                                    <td>{{$user->name}}</td>
                                    <td>
                                        <a href="{{ url('admin/users',$user->id) }}" title="تعديل">
                                            <span class="badge bg-orange">تعديل</span>
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.user.delete',$user->id) }}" method="post" accept-charset="utf-8">
                                             {!! csrf_field() !!}
                                            {{ method_field('DELETE') }}
                                              <button type="submit" class="sub badge bg-red"
                                              style=" border:none;outline:none;"
                                              >حذف</button>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="#" title="">تعديل</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    {{--/ all section --}}
                    </div><!--/.col (left) -->
                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->
@include('admin.layouts.footer')
