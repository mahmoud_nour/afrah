{{--  
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
  --}}
@extends('front.index')

@section('content')
<!-- Form Search  -->
@include('front.layouts.form-search')
<!-- End the Form  -->

<!-- Services  -->
@include('front.layouts.services')
<!-- End the Services  -->
<hr>
<!-- News  -->
@include('front.layouts.news')
<!-- End The News  -->
@endsection

