<?php

Route::get('/user-settings', function () {
    return view('front.user-settings');
});
Route::get('/activation', function () {
    return view('front.activation')->name('activation');
});
// Route::get('/nexmo', function () {

// $nexmo = App('Nexmo\Client');
//         $nexmo->message()->send([
//             'to' => '201147170572 ',
//             'from' => '201147170572 ',
//             'text' => 'Hello from Nexmo'
//         ]);
// });

Route::resource('/nexmo','NexmoController');


Route::resource('sections','SectionController');

Route::get('/', function () {

	// $service = \App\Service::get();
        $services  = \App\Service::whereNotNull('publish')->get();
        // dd($services);
    return view('welcome',compact('services'));
});

Route::get('service/{id}/edit', 'ServiceController@edit')->name('editservice');
Route::get('service/{id}', 'ServiceController@show')->name('showservice');
Route::put('service/{id}', 'ServiceController@update')->name('service.update');
Route::delete('service/{id}/image', 'ServiceController@delete_image')->name('service.delete_image');
Route::delete('service/{id}/destroy', 'ServiceController@delete')->name('service.delete');
Route::post('addService', 'ServiceController@store');
Route::get('/addservice', 'ServiceController@index')->name('addservice');
// Route::resource('/image', 'ImageController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('verifyEmail','Auth\RegisterController@verifyEmail')->name('verifyEmail');
Route::get('verify/{email}/{verifytoken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');
//Admin
Route::get('admin/editor','EditorController@index');
Route::get('admin/home','AdminController@index')->name('admin.home');
Route::get('admin/login','Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login','Admin\LoginController@login');
Route::POST('admin/password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::GET('admin/password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request'); 
Route::POST('admin/password/reset','Admin\ResetPasswordController@reset');
Route::GET('admin/password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin/logout', 'admin\LoginController@logout')->name('admin.logout');

//section CRUD
Route::get('admin/section','AdminController@section')->name('admin.section');
Route::post('admin/section/sectionStore','AdminController@sectionStore')->name('admin.section.store');
Route::get('admin/section/add','AdminController@sectionAdd')->name('admin.section.add');
Route::get('admin/section/{id}','AdminController@sectionEdit')->name('admin.section.edit');
Route::put('admin/section/{id}','AdminController@sectionUpdate')->name('admin.section.Update');
Route::delete('admin/section/{id}', 'AdminController@sectionDelete')->name('admin.section.delete');

//countries CRUD
Route::get('admin/countries','AdminController@countries')->name('admin.countries');
Route::get('admin/countries/add','AdminController@countriesAdd')->name('admin.country.add');
Route::post('admin/countries/store','AdminController@countriesStore')->name('admin.country.store');
Route::put('admin/countries/{id}','AdminController@countriesUpdate')->name('admin.country.Update');
Route::get('admin/countries/{id}','AdminController@countriesEdit')->name('admin.country.edit');
Route::delete('admin/countries/{id}', 'AdminController@countriesDelete')->name('admin.country.delete');

//cities CRUD
Route::get('admin/cities','AdminController@cities')->name('admin.cities');
Route::get('admin/cities/add','AdminController@citiesAdd')->name('admin.city.add');
Route::post('admin/cities/store','AdminController@citiesStore')->name('admin.city.store');
Route::put('admin/cities/{id}','AdminController@citiesUpdate')->name('admin.city.Update');
Route::get('admin/cities/{id}','AdminController@citiesEdit')->name('admin.city.edit');
Route::delete('admin/cities/{id}', 'AdminController@citiesDelete')->name('admin.city.delete');

//services CRUD
Route::get('admin/services','AdminController@services')->name('admin.services');
Route::get('admin/services/add','AdminController@servicesAdd')->name('admin.service.add');
Route::post('admin/services/store','AdminController@servicesStore')->name('admin.service.store');
Route::put('admin/services/{id}','AdminController@servicesUpdate')->name('admin.service.Update');
Route::get('admin/services/{id}','AdminController@servicesEdit')->name('admin.service.edit');
Route::delete('admin/services/{id}', 'AdminController@servicesDelete')->name('admin.service.delete');
//users CRUD
Route::get('admin/users','AdminController@users')->name('admin.users');
Route::get('admin/users/add','AdminController@usersAdd')->name('admin.user.add');
Route::post('admin/users/store','AdminController@usersStore')->name('admin.user.store');
Route::put('admin/users/{id}','AdminController@usersUpdate')->name('admin.user.Update');
Route::get('admin/users/{id}','AdminController@usersEdit')->name('admin.user.edit');
Route::delete('admin/users/{id}', 'AdminController@usersDelete')->name('admin.user.delete');