<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public function services()
        {
            return $this->hasMany(Service::class);
        }
    public function profile()
        {
            return $this->hasOne(Profile::class);
        }
        public function roles()
        {
            return $this->belongsToMany(Role::class);
        }

        public function hasRole($role)
        {
            if (is_role($role)) {
                return $this->roles->contains('name', $role);
            }
            return !! $role->intersect($this->roles)->count();
            // foreach ($role as $r) {
            //     if ($this->hasRole($r->name)) {
            //         return true;
            //     }
            // }
            // return false;
        }
        public function assign($role)
        {
            if (is_string($role)) {
                return $this->roles()->save(
                    Role::whereName($role)->firstOrFail()
                );
            }
            return $this->roles()->save($role);
        }


    protected $fillable = [
        'name', 'email', 'password','status','phone_number','verify_mob','verified'
        // ,'verifytoken'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
