<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $fillable =[
		'hall','user_id','publish',
		'section_id','country_id','city_id',
		'slug','area','street','address','phone','email','web','C_register','notes','price'
	];
    public function images(){
	    return $this->belongsToMany('App\Image','service_image');
	}
	public function getRouteKeyName()
	{
		return 'name';
	}
	public function section()
	{
		return $this->belongsTo('App\Section');
	}
	public function city()
	{
		return $this->belongsTo('App\City');
	}
	public function country()
	{
		return $this->belongsTo('App\Country');
	}
}
