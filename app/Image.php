<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $fillable =['name','path'];
    public function services(){
        return $this->belongsToMany('App\Service','service_image'); //table
    }
}
