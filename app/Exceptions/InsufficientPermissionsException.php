<?php
namespace App\Exceptions;

class InsufficientPermissionsException extends \RuntimeException
{
}
