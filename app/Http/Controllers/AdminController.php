<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Section;
use App\Service;
use App\User;
use App\City;
use App\Country;
use Session;
class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->middleware('admin');
    }
    public function index(){
    	$services =Service::all();
    	$users =User::all();
    	$sections =Section::all();
        return view('admin.index',compact('services','users','sections'));
    }

    public function section(){
    	$sections = Section::all();
    	return view('admin.section.index',compact('sections'));	
    }
    public function sectionAdd(){
    	$section=Section::all();
    	return view('admin.section.add');	
    }
    public function sectionStore(request $request){
    	$section=Section::create([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	Session::flash('message', ' تم الاضافه !');
    	// return view('admin.section.index');	
    	return redirect()->back();	
    }
    public function sectionEdit($id){
    	$section=Section::find($id);
    	return view('admin.section.edit',compact('section'));	
    }
    public function sectionUpdate($id,request $request){
    	$section=Section::where('id',$id)->first();
    	$section->update([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	$section->save();
    	Session::flash('message', ' تم تعديل !');
    	return redirect()->back();	
    }
    public function sectionDelete($id){
    	$section = Section::find($id);
    	$section->delete();
    	Session::flash('message', ' تم الحذف');
    	return redirect('admin/section');
    }

    public function countries(){
    	$countries = Country::all();
    	return view('admin.countries.index',compact('countries'));
    }
    public function countriesAdd(){

    	return view('admin.countries.add');
    }
    public function countriesStore(request $request){
    	$country=Country::create([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	Session::flash('message', ' تم الاضافه !');
    	// return view('admin.section.index');	
    	return redirect()->back();
    }
    public function countriesUpdate($id,request $request){
    	$section=Country::where('id',$id)->first();
    	$section->update([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	$section->save();
    	Session::flash('message', ' تم تعديل !');
    	return redirect()->back();	
    }
    public function countriesEdit($id){
    	$country=Country::find($id);
    	return view('admin.countries.edit',compact('country'));	
    }
    public function countriesDelete($id){
    	$country = Country::find($id);
    	$country->delete();
    	Session::flash('message', ' تم الحذف');
    	return redirect('admin/countries');
    }

    public function cities(){
    	$cities = City::all();
    	return view('admin.cities.index',compact('cities'));
    }
    public function citiesAdd(){

    	return view('admin.cities.add');
    }
    public function citiesStore(request $request){
    	$city=City::create([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	Session::flash('message', ' تم الاضافه !');
    	// return view('admin.section.index');	
    	return redirect()->back();
    }
    public function citiesUpdate($id,request $request){
    	$section=City::where('id',$id)->first();
    	$section->update([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	$section->save();
    	Session::flash('message', ' تم تعديل !');
    	return redirect()->back();	
    }
    public function citiesEdit($id){
    	$city=City::find($id);
    	return view('admin.cities.edit',compact('city'));	
    }
    public function citiesDelete($id){
    	$city = City::find($id);
    	$city->delete();
    	Session::flash('message', ' تم الحذف');
    	return redirect('admin/cities');
    }


    public function services(){
    	$services = Service::all();
    	return view('admin.services.index',compact('services'));
    }
    public function servicesAdd(){

    	return view('admin.services.add');
    }
    public function servicesStore(request $request){
    	$service=Service::create([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	Session::flash('message', ' تم الاضافه !');
    	// return view('admin.section.index');	
    	return redirect()->back();
    }
    public function servicesUpdate($id,request $request){
    	$section=Service::where('id',$id)->first();
    	$section->update([
    	'name' => $request->name,
    	'slug' => str_slug($request->name)
    	]);
    	$section->save();
    	Session::flash('message', ' تم تعديل !');
    	return redirect()->back();	
    }
    public function servicesEdit($id){
    	$service=Service::find($id);
    	return view('admin.services.edit',compact('service'));	
    }
    public function servicesDelete($id){
    	$service = Service::find($id);
    	$service->delete();
    	Session::flash('message', ' تم الحذف');
    	return redirect('admin/services');
    }

    public function users(){
    	$users = User::all();
    	return view('admin.users.index',compact('users'));
    }
    public function usersAdd(){

    	return view('admin.users.add');
    }
    public function usersStore(request $request){
    	$user=User::create([
    	'name' => $request->name,
    	'email' => $request->email,
    	'password' => Hash::make($request->password),
    	'status' => $request->status
    	]);
    	Session::flash('message', ' تم الاضافه !');
    	// return view('admin.section.index');	
    	return redirect()->back();
    }
    public function usersUpdate($id,request $request){
    	$user=User::where('id',$id)->first();
    	if(Hash::check(request('password'),$user->password)){
			$user->name 	= request('name') ?? $user->name;
			$user->status 	= request('status') ?? $user->status;
			$user->email    = request('email') ?? $user->email;
			$user->is_admin    = request('is_admin') ?? $user->is_admin;
			$user->save();
			Session::flash('message', ' تم تعديل !');
			return redirect()->back();
		}else{
			Session::flash('message', 'جميع الخانات مطلوبه !');
			return redirect()->back();
		
		}
    	
    	// $user->update([
    	// 'name' => $request->name,
    	// 'email' => $request->email, 
    	// 'password' => Hash::make($request->password),
    	// 'status' => $request->status
    	// ]);
    	// $user->save();
    	// Session::flash('message', ' تم تعديل !');
    	// return redirect()->back();	
    }
    public function usersEdit($id){
    	$user=User::find($id);
    	return view('admin.users.edit',compact('user'));	
    }
    public function usersDelete($id){
    	$user = User::find($id);
    	$user->delete();
    	Session::flash('message', ' تم الحذف');
    	return redirect('admin/users');
    }
}
