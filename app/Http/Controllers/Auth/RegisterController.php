<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Mail;
use App\Mail\verifyEmail;
use Session;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/nexmo';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data);
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone_number' => 'required',
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        Session::flash('status','Registered Done please verify to activate account');
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'password' => bcrypt($data['password']),
            // 'verifytoken'=> Str::random(40),
            'verify_mob'=> Str::random(5),
        ]);
        $thisUser = User::findOrFail($user->id);
        // $this->sendEmail($thisUser);
        $nexmo = App('Nexmo\Client');
        $nexmo->message()->send([
            'to' => $thisUser->phone_number,
            'from' => '201147170572',
            'text' => 'afrah.com activate code is   '.'  '.$thisUser->verify_mob.' '.'  '
                // 'text' => 'afrah.com كود التفعيل الخاص بك هو '.$thisUser->verify_mob
        ]);
        // return $user;
        redirect('/nexmo');
    }

    // public function sendEmail($thisUser)
    // {
    //     Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
    // }
    // public function verifyEmail()
    // {
    //     return view('email.verifyEmail');
    // }
    // public function sendEmailDone($email ,$verifytoken)
    // {
    //     $user = User::where(['email'=>$email,'verifytoken'=>$verifytoken])->first();
    //     if ($user) {
    //         return User::where(['email'=>$email,
    //             'verifytoken'=>$verifytoken])->update(['status'=>'1',
    //             'verifytoken'=>Null]);
    //     }else {
    //         return 'user not found';
    //     }
    // }
}
