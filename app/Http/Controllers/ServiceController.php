<?php

namespace App\Http\Controllers;
use App\Service;
use App\City;
use App\Section;
use App\Country;
use App\Image;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
class ServiceController extends Controller
{   
    public function index(Service $service )
    {
        $cities = City::get();
        $sections = Section::get();
        $countries = Country::get();
        return view('front.AddService',compact('services','cities','countries','sections'));
    }

    public function store(Request $request)
    {
        $service = Service::create([
            'hall'      => Input::get('hall'),
            'user_id' => auth()->user()->id,
            'slug'      => str_slug(Input::get('hall')),
            'section_id'   => Input::get('section'),
            // 'section_id'   => Input::get('section'),
            'country_id'   => Input::get('country'),
            'city_id'      => Input::get('city'),
            'price'     => Input::get('price'),
            'area'      => Input::get('area'),
            'street'    => Input::get('street'),
            'address'   => Input::get('address'),
            'phone'     => Input::get('phone'),
            'email'     => Input::get('email'),
            'web'       => Input::get('web'),
            'C_register'=> Input::get('C_register'),
            'notes'     => Input::get('notes')
        ]);
    	$images = $request->file('images'); 
    	if(!empty($images)):
        foreach ($images as $image) {
            $move = $image->move('uploads/service_images',$image->getClientOriginalName());
            if ($move) {
                $imgaedata = Image::create([
                'name' => $image->getClientOriginalName(),
                'path' => '/uploads/service_images/',
            ]);
            }
            $service->images()->attach([$imgaedata->id]);
        }
    	endif;
        $service->save();
        // var_dump($images);
        Session::flash('message', ' تم اضافة خدمة جديدة بنجاح!');
    	return redirect()->back();
    }

    public function show($id)
    {
        $service = Service::findOrFail($id);
        return view('front.service.show',compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::where('id',$id)->first();

            return view('front.service.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Service $service,Image $image, $id)
    { 
        $service = Service::with('images')->where('id',$id)->first(); 
        $service->update([
            'hall'      => request('hall'),
            'slug'      => str_slug(request('hall')),
            'section'   => request('section'),
            'section'   => request('section'),
            'country'   => request('country'),
            'city'      => request('city'),
            'price'     => request('price'),
            'area'      => request('area'),
            'street'    => request('street'),
            'address'   => request('address'),
            'phone'     => request('phone'),
            'email'     => request('email'),
            'web'       => request('web'),
            'C_register'=> request('C_register'),
            'notes'     => request('notes')
        ]);
        $images = $request->file('images'); 
        if(!empty($images)):
        foreach ($images as $image) {
            $move = $image->move('uploads/service_images',$image->getClientOriginalName());
            if ($move) {
                $imgaedata = Image::create([
                'name' => $image->getClientOriginalName(),
                'path' => '/uploads/service_images/',
            ]);
            }
            $service->images()->attach([$imgaedata->id]);
        }
        endif;
        $service->save();
        Session::flash('message', ' تم اضافة خدمة جديدة بنجاح!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service = Service::find($id);
        $service->delete();
        Session::flash('message', ' delete');
        return redirect('/');
    }
    public function delete_image($id,Request $request)
    {
        $service = Service::find($id);
        $service->images()->detach();
        Session::flash('message', ' تم حذف الصور');
        return redirect()->back();

    }
}
