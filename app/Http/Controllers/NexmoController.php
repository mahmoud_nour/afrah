<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Validator;
class NexmoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $nexmo = App('Nexmo\Client');
        // $nexmo->message()->send([
        //     'to' => '201147170572 ',
        //     'from' => '201147170572 ',
        //     'text' => 'Hello from Nexmo'
        // ]);

        return view('front.activation');

        // $this->validate($request, [
        //     'phone_number' => 'required|regex:/[0-9]{11}/|digits:11',            
        // ]);

        // $user = User::where('phone_number', $request->get('phone_number'))->first();

        //  // Check Condition Mobile No. Found or Not
        // if($request->get('phone_number') != $user->phone_number) {
        //     \Session::put('errors', 'Your mobile number not match in our system..!!');
        //     return back();
        // }  
        // $nexmo = App('Nexmo\Client');
        // $nexmo->message()->send([
        //     'to' => '201147170572 ',
        //     'from' => '201147170572 ',
        //     'text' => 'Hello from Nexmo'
        // ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'verify_mob' => 'required|max:5|min:4',
        ]);
        
    $user = User::where(['verify_mob'=>$request->verify_mob])->first();
     // dd($user);
     if ($user) {
            User::where(
                ['verify_mob'=>$request->verify_mob]
            )->update(['verified'=>'1','verify_mob'=>null]);
            Session::flash('message', 'ـتم تفعيل العضو بنجاح');
            return redirect('/login');
        }else {
            Session::flash('message', 'خطأ');
            return redirect()->back();
        }
    // $nexmo = App('Nexmo\Client');
    // $nexmo->message()->send([
    //     'to' => '201147170572 ',
    //     'from' => '201147170572 ',
    //     'text' => 'Hello from Nexmo'
    // ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
